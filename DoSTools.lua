local DoSTools, PezUtil, PezColor = PezLib:GetAddonComponents(...);

-- Define local constants
local COMMAND_ROOT = "dos";

-- Store addon globally
_G.DoSTools = DoSTools;


--============================================================================--
-- Initialization
--============================================================================--

--- Called when the addon has fully loaded and initialized
function DoSTools:OnInitialize()

	-- Test mode is always off on load
	self.test = false;
end



--============================================================================--
-- Console commands
--============================================================================--

-- Register root command
local dosCommand = DoSTools:RegisterCommand(COMMAND_ROOT);

-- Register 'test' command to enable test mode
dosCommand:AddCommand("test", "Toggle test mode", function()
	local self = DoSTools;

	-- Toggle test mode
	self.test = not self.test;

	-- Print state change message
	local enabled = PezColor.ON("enabled");
	local disabled = PezColor.OFF("disabled");
	self:Print("Test mode " .. (self.test and enabled or disabled));
end);