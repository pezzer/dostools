## Interface: 70200
## Title: |cFFABD473DoS Tools|r
## Notes: A collection of raid tools for Denial of Service
## Author: Pezzer
## Version: 0.0.1
## SavedVariables: DoSToolsDB

embeds.xml

DoSTools.lua
modules/battle-rez.lua
modules/guild-note.lua