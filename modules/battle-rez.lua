local DoSTools, PezUtil, PezColor = PezLib:GetAddonComponents(...);

-- Cache globals
local LibStub = LibStub;
local UIParent = UIParent;
local GetTime = GetTime;
local GetSpellCharges = GetSpellCharges;
local CreateFrame = CreateFrame;
local max = max;
local floor = floor;
local format = format;
local unpack = unpack;


-- Define local constants
local FRAME_NAME = "Frame";
local REBIRTH_SPELL_ID = 20484;
local PADDING = 5;
local LABEL_MARGIN = 2;
local LINE_SPACING = 5;


-- Load required libs
local AceTimer = LibStub("AceTimer-3.0");


-- Create module
local module = DoSTools:NewModule("BattleRez");

-- Configure defaults
module:SetDefaults({
	lock = false,
	left = 100,
	top = 100,
	font = "GameFontNormal",
	fontSize = 16,
	outline = "OUTLINE",
	fontLayer = "OVERLAY",
	color = PezColor.WHITE:ToColorTable(),
	chargesColor = PezColor.WHITE:ToColorTable(),
	lowChargesThreshold = 0,
	lowChargesColor = PezColor.BAD:ToColorTable(),
	timeColor = PezColor.WHITE:ToColorTable(),
	shortTimeThreshold = 90,
	shortTimeColor = PezColor.GOOD:ToColorTable()
});



--============================================================================--
-- Module functions
--============================================================================--

--- Called when the module has been initialized.
function module:OnInitialize()

	-- Create frame
	self:CreateFrame();
end

--- Called when the module has been enabled.
function module:OnEnable()

	-- Register timer to update state
	self.updateTimer = AceTimer:ScheduleRepeatingTimer(function()
		module:Update();
	end, 0);
end

--- Called when the module has been disabled.
function module:OnDisable()

	-- Cancel update timer
	AceTimer:CancelTimer(self.updateTimer);
end



--============================================================================--
-- Functions
--============================================================================--

--- Creates the root UI / update frame.
function module:CreateFrame()

	-- Create frame
	local frame = CreateFrame("Frame", self:Prefix(FRAME_NAME), UIParent);
	self.frame = frame;

	-- Configure frame
	PezUtil.setScreenPoint(frame, self.db.left, self.db.top);
	frame:SetFrameStrata("HIGH");
	frame:SetClampedToScreen(true);


	-- Configure dragging
	frame:RegisterForDrag("LeftButton");
	frame:SetScript("OnDragStart", function(self)
		if self:IsMovable() then
			self:StartMoving();
		end
	end)
	self.frame:SetScript("OnDragStop", function(self)
		self:StopMovingOrSizing();
		module.db.left = self:GetLeft();
		module.db.top = self:GetTop();
	end)

	-- Enable/disable dragging
	self:UpdateFrameLock();


	-- Content frame
	frame.content = CreateFrame("Frame", nil, frame)
	frame.content:SetAllPoints()
	frame.content:SetFrameLevel(50)


	-- Charges label
	frame.chargesLabel = self:CreateFontString(frame.content);
	frame.chargesLabel:SetPoint("TOPLEFT", PADDING, -PADDING);
	frame.chargesLabel:SetText("BATTLE REZ:");

	-- Charges value
	frame.charges = self:CreateFontString(frame.content);
	frame.charges:SetPoint("LEFT", frame.chargesLabel, "RIGHT", LABEL_MARGIN, 0);
	frame.charges:SetTextColor(unpack(self.db.chargesColor));


	-- Time label
	frame.timeLabel = self:CreateFontString(frame.content);
	frame.timeLabel:SetPoint("BOTTOMLEFT", PADDING, PADDING);
	frame.timeLabel:SetText("NEXT IN:");

	-- Time value
	frame.time = self:CreateFontString(frame.content);
	frame.time:SetPoint("LEFT", frame.timeLabel, "RIGHT", LABEL_MARGIN, 0);
	frame.time:SetTextColor(unpack(self.db.timeColor));


	-- Hide frame until relevant
	frame:Hide()
end

--- Creates a font string for use in the display.
-- @param parent (Frame) Frame to be the parent of the created font string
-- @return (FontString) The created font string
function module:CreateFontString(parent)
	local fontString = parent:CreateFontString(nil, "OVERLAY", self.db.font);
	fontString:SetFont(fontString:GetFont(), self.db.fontSize, self.db.outline);
	fontString:SetTextColor(unpack(self.db.color));
	return fontString;
end

--- Resizes the main frame to fit the contained text.
function module:ResizeFrame()

	-- Compute the width of the content
	local line1Width = self.frame.chargesLabel:GetStringWidth() + self.frame.charges:GetStringWidth() + LABEL_MARGIN;
	local line2Width = self.frame.timeLabel:GetStringWidth() + self.frame.time:GetStringWidth() + LABEL_MARGIN;
	local contentWidth = max(line1Width, line2Width) + (PADDING * 2);

	-- Compute the height of the content
	local contentHeight = self.frame.chargesLabel:GetStringHeight() + self.frame.timeLabel:GetStringHeight() + LINE_SPACING + (PADDING * 2);

	-- Resize the frame
	self.frame:SetSize(contentWidth, contentHeight);
end

--- Locks/unlocks the frame in position, based on current option settings.
function module:UpdateFrameLock()
	self.frame:EnableMouse(not self.db.locked);
	self.frame:SetMovable(not self.db.locked);
end

--- Returns the current state of battle rez charges.
-- @return (number) Number of battle rez charges available, nil if not applicable
-- @return (number) Maximimum number of battle rez charges available, nil if not applicable
-- @return (number) Number of seconds until the next charge will be available, nil if not applicable
function module:GetBattleRezInfo()

	-- Check charges/cooldown of a brez spell (Rebirth in this case, but all brez spells share charges)
    local charges, maxCharges, started, duration = GetSpellCharges(REBIRTH_SPELL_ID);

	-- If no charges possible, short-circuit
	if (not charges) then
		return;
	end


    -- Compute time to next charge
    local timeToNextCharge = duration - (GetTime() - started)

    -- Return battle rez info
    return charges, maxCharges, timeToNextCharge;
end

--- Returns mock battle rez info used for testing the module.
-- @return (number) Number of battle rez charges available, nil if not applicable
-- @return (number) Maximimum number of battle rez charges available, nil if not applicable
-- @return (number) Number of seconds until the next charge will be available, nil if not applicable
function module:GetMockBattleRezInfo()
	local time = GetTime();

	-- Initialize test state
	self.testState = self.testState or {
		startTime = time;
		timeToNextCharge = 300;
	};

	-- Set max charges
	local maxCharges = 3;

	-- Cycle charge count
	local chargeStates = (maxCharges * 2);
	local chargeState = (floor(time - self.testState.startTime) % chargeStates);
	local charges = (chargeState > maxCharges) and (chargeStates - chargeState) or chargeState;

	-- Cycle timer (artificially decrease duration to speed it up)
	local timeToNextCharge = (self.testState.timeToNextCharge - 1.5);
	if (timeToNextCharge < 0) then
		self.testState.startTime = time;
		timeToNextCharge = (timeToNextCharge + 300);
	end
	self.testState.timeToNextCharge = timeToNextCharge;

	-- Return mock state
	return charges, maxCharges, timeToNextCharge;
end

--- Updates battle rez state and display.
function module:Update()

	-- Get latest brez info
	local charges, maxCharges, timeToNextCharge = self:GetBattleRezInfo();


	-- If test flag is set, update brez info with test state,
	-- otherwise, clear test state
	if (DoSTools.test) then
		charges, maxCharges, timeToNextCharge = self:GetMockBattleRezInfo();
	elseif (self.testState) then
		self.testState = nil;
	end


	-- If no charges possible, hide the frame and short-circuit
	if (not charges) then
		self.frame:Hide();
		return;
	end


	-- Update frame lock state
	self:UpdateFrameLock();


	-- Update brez charge state
	self.charges = charges;
	self.maxCharges = maxCharges;
	self.timeToNextCharge = timeToNextCharge;

	-- Update display text
	self.frame.charges:SetText(self.charges);
	self.frame.time:SetText(format("%d:%02d", floor(self.timeToNextCharge / 60), (self.timeToNextCharge % 60)));

	-- Set color of charge count
	local chargesColor = (charges > self.db.lowChargesThreshold) and self.db.chargesColor or self.db.lowChargesColor;
	self.frame.charges:SetTextColor(unpack(chargesColor));

	-- Set color of time
	local timeColor = (timeToNextCharge <= self.db.shortTimeThreshold) and self.db.shortTimeColor or self.db.timeColor;
	self.frame.time:SetTextColor(unpack(timeColor));


	-- Resize frame to fit, and show
	self.frame:Show();
	self:ResizeFrame();
end