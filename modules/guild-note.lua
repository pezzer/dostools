local DoSTools, PezUtil, PezColor = PezLib:GetAddonComponents(...);

-- Cache globals
local LibStub = LibStub;
local GuildRoster = GuildRoster;
local GetNumGuildMembers = GetNumGuildMembers;
local GetGuildRosterInfo = GetGuildRosterInfo;
local GetPlayerInfoByGUID = GetPlayerInfoByGUID;
local UnitFullName = UnitFullName;
local pcall = pcall;
local gsub = gsub;
local string = string;
local select = select;
local CUSTOM_CLASS_COLORS = CUSTOM_CLASS_COLORS;
local RAID_CLASS_COLORS = RAID_CLASS_COLORS;


-- Load required libs
local AceHook = LibStub("AceHook-3.0");


-- Create module
local module = DoSTools:NewModule("GuildNote");



--============================================================================--
-- Module functions
--============================================================================--

--- Called when the module has been initialized.
function module:OnInitialize()

	-- Initialize guild roster data
	GuildRoster();

	-- Hook Chat functions to update player names as chat messages come in
	self:HookChatFunctions();
end


--- Sets up required hooks to update the names of characters appearing in chat.
function module:HookChatFunctions()

	-- Hook GetColoredName (called whenever a name is displayed in chat)
	-- in order to add the guild note to the player name
	AceHook:RawHook("GetColoredName", function(event, ...)
		local authorGuid = select(12, ...);

		-- Invoke original function to process
		local coloredName = AceHook.hooks.GetColoredName(event, ...);


		-- Get guild note
		local note = module:GetGuildNote(authorGuid);


		-- If there's no colored name or note, we're done
		if (not coloredName or not note or note == "") then
			return coloredName;
		end


		-- Determine if class coloring is enabled by looking for color escape
		local classColorEnabled = string.match(coloredName, "|c.*");

		-- Attempt to color note with class color
		if (note and classColorEnabled) then

			-- Search guild roster for class of character matching the note
			local noteClass = module:GetGuildMemberClass(note, authorGuid);

			-- If class found for note, find the class color
			if (noteClass) then
				local noteColor = CUSTOM_CLASS_COLORS and CUSTOM_CLASS_COLORS[noteClass] or RAID_CLASS_COLORS[noteClass];

				-- If note color was successfuly created, colorize the note
				if (noteColor) then
					note = PezColor:New(noteColor):Colorize(note);
				end
			end
		end


		-- Handle emotes differently (there aren't square brackets)
		if (event == "CHAT_MSG_TEXT_EMOTE") then
			return module:AddNoteToEmoteName(coloredName, note);
		end

		-- Add guild note and return
		return module:AddNoteToName(coloredName, note);
	end, true);
end


---
-- Retrieves guild note for the character with a given GUID. This function only works if GuildRoster() has
-- been invoked and the GUILD_ROSTER_UPDATE event has subsequently fired.
-- @param characterGuid (string) GUID of the character
-- @return (string) The guild note set for the specified character
function module:GetGuildNote(characterGuid)

	-- Don't bother looking if no GUID was provided
	if (not characterGuid) then
		return;
	end


	-- Get number of guild members
	local numGuildMembers = GetNumGuildMembers();

	-- Check each guild roster index for matching player
	for i = 1, numGuildMembers do

		-- Get guild roster info for index
		local fullName, _, _, _, _, _, note = GetGuildRosterInfo(i);

		-- Get author name and realm
		local _, _, _, _, _, _, name, realm = pcall(GetPlayerInfoByGUID, characterGuid);

		-- If no realm returned, use player's realm
		if (not realm or realm == "") then
			_, realm = UnitFullName("PLAYER");
		end

		-- If current guild member matches the character, return the guild note
		if (name and realm) then
			local nameAndRealm = name .. "-" .. gsub(realm, "[%s%-]", "");
			if (fullName == nameAndRealm) then
				return note;
			end
		end
	end
end


---
-- Searches the guild roster for a character matching the guild note and returns that character's note.
-- If multiple characters are found with matching names (on different realms), a character on the same realm
-- as the author will be preferred.
-- @param characterName (string) Character name to search for (without realm)
-- @param authorGuid (string) GUID of the author of the chat message in which the character name was found
-- @return (string) Class identifer of the matched character (referred to as 'classFilename' in Blizzard APIs)
function module:GetGuildMemberClass(characterName, authorGuid)
	local bestClassMatch;

	-- Get number of guild members
	local numGuildMembers = GetNumGuildMembers();

	-- Check each guild roster index for matching player
	for i=1,numGuildMembers do

		-- Get guild roster info for index
		local fullName, _, _, _, _, _, _, _, _, _, class = GetGuildRosterInfo(i);

		-- Separate name and realm name
		local name, realm = string.match(fullName, "(.*)-(.*)");


		-- Verify current guild member name matches target name
		if (name == characterName) then

			-- Store class in case a name and realm match aren't found together
			bestClassMatch = class;

			-- Get author name and realm
			local _, _, _, _, _, _, _, authorRealm = pcall(GetPlayerInfoByGUID, authorGuid);

			-- If no author realm returned, use player's realm
			local targetRealm = (authorRealm ~= "" and authorRealm or select(2, UnitFullName("PLAYER")));

			-- If target realm matches current guild member's realm, assume that's the proper character
			if (realm == gsub(targetRealm, "[%s%-]", "")) then
				return class;
			end
		end
	end

	-- No matching name was found with matching realm, so return class from matching name only
	return bestClassMatch;
end


--- Attaches a given note to a given name string to construct a new string appropriate for display in standard chat messages.
-- @param name (string) Character name
-- @param note (string) Guild note
-- @param (string) Updated name string containing guild note, for display in chat
function module:AddNoteToName(name, note)

	-- Ensure the note hasn't already been added
	if (string.match(name, ".*%] %[" .. note .. ".*")) then
		return name;
	end

	-- Construct and return the string
	return name .. "] [" .. note;
end

--- Attaches a given note to a given name string to construct a new string appropriate for display in an emote.
-- @param name (string) Character name
-- @param note (string) Guild note
-- @param (string) Updated name string containing guild note, for display in an emote
function module:AddNoteToEmoteName(name, note)

	-- Ensure the note hasn't already been added
	if (string.match(name, ".* %(" .. note .. ").*")) then
		return name;
	end

	-- Construct and return the string
	return name .. " (" .. note .. ")";
end